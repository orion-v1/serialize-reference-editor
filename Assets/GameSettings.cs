﻿using Orion;
using System;
using System.Collections.Generic;
using UnityEngine;
using Orion.SerializeReference;

namespace TestApp
{
    [CreateAssetMenu(fileName = "GameSettings.asset", menuName = "GameSettings")]
    public class GameSettings : ScriptableObject
    {
        [SerializeReference]
        [SerializeReferenceEditor]
        //[InheritedObjectFactory(BaseType = typeof(Cube))]
        private IFigure _figure;

        [SerializeReference]
        [SerializeReferenceEditor]
        private List<IFigure> _figures;
    }

    public interface IFigure
    {
        void Log();
    }


    [Serializable]
    public class Cube : IFigure
    {
        public int Size;
        public Color Color = Color.white;

        public void Log() => Debug.Log("Cube");
    }

    [Serializable]
    public class Cube2 : Cube
    {
        public int Size2;
    }

    [Serializable]
    public class Sphere : IFigure
    {
        public int Radius;
        public Color Color = Color.white;

        public void Log() => Debug.Log("Sphere");
    }

    [Serializable]
    public class Cylinder : IFigure
    {
        public int Radius;
        public int Height;
        public Color Color = Color.white;

        public void Log() => Debug.Log("Cylinder");
    }
}
