﻿namespace Orion.SerializeReference.Editor
{
    public interface IObjectDescriptor
    {
        string Description { get; }
    }
}
