﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Orion.SerializeReference.Editor
{
    public interface IObjectFactory
    {
        FieldInfo FieldInfo { get; set; }

        IEnumerable<IObjectDescriptor> GetObjectDescriptors(Type targetType);

        object CreateObject(IObjectDescriptor descriptor);
    }
}
