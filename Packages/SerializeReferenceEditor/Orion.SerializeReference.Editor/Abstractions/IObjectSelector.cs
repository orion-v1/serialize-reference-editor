﻿using System.Collections.Generic;

namespace Orion.SerializeReference.Editor
{
    public interface IObjectSelector
    {
        void Show(IObjectSelectorContext context);
    }
}
