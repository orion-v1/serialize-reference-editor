﻿using System;
using UnityEngine;

namespace Orion.SerializeReference.Editor
{
    public interface IObjectSelectorContext
    {
        Type TargetType { get; }
        IObjectFactory ObjectFactory { get; }
        Rect PropertyRect { get; }

        void SetResult(IObjectDescriptor descriptor);
    }
}
