﻿using System;
using System.Reflection;

namespace Orion.SerializeReference.Editor
{
    internal interface ISerializeReferenceProperty
    {
        FieldInfo FieldInfo { get; }
        Type TargetType { get; }
        bool IsCollection { get; }
        IObjectFactory ObjectFactory { get; }
        IObjectSelector ObjectSelector { get; }
    }
}
