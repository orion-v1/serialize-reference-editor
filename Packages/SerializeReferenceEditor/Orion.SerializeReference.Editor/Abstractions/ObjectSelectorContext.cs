﻿using System;
using UnityEditor;
using UnityEngine;

namespace Orion.SerializeReference.Editor
{
    public class ObjectSelectorContext : IObjectSelectorContext
    {
        private Action<IObjectSelectorContext, IObjectDescriptor> _callback;

        public IObjectFactory ObjectFactory { get; }
        public Type TargetType { get; set; }
        public SerializedProperty SerializedProperty { get; }
        public Rect PropertyRect { get; }


        public ObjectSelectorContext(
            IObjectFactory objectFactory,
            Type targetType,
            SerializedProperty serializedProperty,
            Rect propertyRect,
            Action<IObjectSelectorContext, IObjectDescriptor> callback)
        {
            ObjectFactory = objectFactory;
            TargetType = targetType;
            SerializedProperty = serializedProperty;
            PropertyRect = propertyRect;
            _callback = callback;
        }

        public void SetResult(IObjectDescriptor descriptor)
        {
            var callback = _callback;
            _callback = null;
            callback?.Invoke(this, descriptor);
        }
    }
}
