﻿using System;

namespace Orion.SerializeReference.Editor
{
    public sealed class ObjectFactoryImplementationAttribute : Attribute
    {
        public Type Type { get; set; }

        public ObjectFactoryImplementationAttribute(Type type)
        {
            Type = type;
        }
    }
}
