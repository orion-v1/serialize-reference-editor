﻿using System;

namespace Orion.SerializeReference.Editor
{
    public sealed class ObjectSelectorImplementationAttribute : Attribute
    {
        public Type Type { get; set; }

        public ObjectSelectorImplementationAttribute(Type type)
        {
            Type = type;
        }
    }
}
