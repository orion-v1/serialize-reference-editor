﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace Orion.SerializeReference.Editor
{
    internal static class ContactResolverUtils
    {
        [NonSerialized] private static Dictionary<Type /*ObjectFactoryImplAttribute*/, Type /*IObjectFactory*/> _objectFactoryTypes;
        [NonSerialized] private static Dictionary<Type /*ObjectFactoryImplAttribute*/, Type /*IObjectFactory*/> _objectSelectorTypes;

        public static IReadOnlyDictionary<Type, Type> ObjectFactoryTypes => _objectFactoryTypes ?? (_objectFactoryTypes = GetObjectFactoryTypes());
        public static IReadOnlyDictionary<Type, Type> ObjectSelectorTypes => _objectSelectorTypes ?? (_objectSelectorTypes = GetObjectSelectorTypes());

        private static Dictionary<Type, Type> GetObjectFactoryTypes()
        {
            var collection = new Dictionary<Type, Type>();

            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(p => p.DefinedTypes);

            foreach (var type in types)
            {
                if (!type.IsClass || type.IsAbstract) continue;

                // should have attribute TypesRegistryImplementation
                var implAttribute = type.GetCustomAttribute<ObjectFactoryImplementationAttribute>(false);
                if (implAttribute == null) continue;

                // should implement interface ITypesRegistry
                if (!type.ImplementedInterfaces.Contains(typeof(IObjectFactory)))
                {
                    Debug.LogError($"Type '{type}' doesn't implement interface {nameof(IObjectFactory)}.");
                    continue;
                }

                collection.Add(implAttribute.Type, type);
            }

            return collection;
        }

        private static Dictionary<Type, Type> GetObjectSelectorTypes()
        {
            var collection = new Dictionary<Type, Type>();

            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(p => p.DefinedTypes);

            foreach (var type in types)
            {
                if (!type.IsClass || type.IsAbstract) continue;

                // should have attribute TypesRegistryImplementation
                var implAttribute = type.GetCustomAttribute<ObjectSelectorImplementationAttribute>(false);
                if (implAttribute == null) continue;

                // should implement interface ITypesRegistry
                if (!type.ImplementedInterfaces.Contains(typeof(IObjectSelector)))
                {
                    Debug.LogError($"Type '{type}' doesn't implement interface {nameof(IObjectSelector)}.");
                    continue;
                }

                collection.Add(implAttribute.Type, type);
            }

            return collection;
        }

        public static IObjectFactory GetObjectFactory(ObjectFactoryAttribute attribute)
        {
            if (attribute == null) return null;

            Type type;
            if (!ObjectFactoryTypes.TryGetValue(attribute.GetType(), out type))
                return null;

            try
            {
                return (IObjectFactory)Activator.CreateInstance(type);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                return null;
            }
        }

        public static IObjectSelector GetObjectSelector(ObjectSelectorAttribute attribute)
        {
            if (attribute == null) return null;

            Type type;
            if (!ObjectSelectorTypes.TryGetValue(attribute.GetType(), out type))
                return null;

            try
            {
                return (IObjectSelector)Activator.CreateInstance(type);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                return null;
            }
        }
    }
}
