﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Orion.SerializeReference.Editor
{
    internal class DefaultContractResolver
    {
        public IObjectFactory GetDefaultObjectFactory() => new InheritedObjectFactory();

        public IObjectSelector GetDefaultObjectSelector() => new PopupMenuObjectSelector();

        public SerializeReferenceProperty ResolveProperty(FieldInfo fieldInfo)
        {
            var attribute = fieldInfo.GetCustomAttribute<SerializeReferenceEditorAttribute>(false);
            if (attribute == null) return null;

            var property = new SerializeReferenceProperty(fieldInfo);
            //property.AllowNull = attribute.AllowNull;

            ResolveTargetType(property);
            ResolveObjectFactory(property);
            ResolveObjectSelector(property);

            property.ObjectFactory.FieldInfo = fieldInfo;
            //property.ObjectSelector.FieldInfo = fieldInfo;

            // TODO: validate

            return property;
        }

        public void ResolveTargetType(SerializeReferenceProperty property)
        {
            Type fieldType = property.FieldInfo.FieldType;
            if (fieldType.IsArray)
            {
                property.IsCollection = true;
                property.TargetType = fieldType.GetElementType();
            }
            else if (fieldType.IsGenericType)
            {
                foreach (Type @interface in fieldType.GetInterfaces())
                    if (@interface.IsGenericType && @interface.GetGenericTypeDefinition() == typeof(IEnumerable<>))
                    {
                        property.IsCollection = true;
                        property.TargetType = fieldType.GetGenericArguments().Single();
                        break;
                    }
            }
            else
            {
                property.IsCollection = false;
                property.TargetType = fieldType;
            }
        }

        public void ResolveObjectFactory(SerializeReferenceProperty property)
        {
            var attribute = property.FieldInfo.GetCustomAttribute<ObjectFactoryAttribute>(true);
            property.ObjectFactory = ContactResolverUtils.GetObjectFactory(attribute) ?? GetDefaultObjectFactory();
        }

        public void ResolveObjectSelector(SerializeReferenceProperty property)
        {
            var attribute = property.FieldInfo.GetCustomAttribute<ObjectSelectorAttribute>(true);
            property.ObjectSelector = ContactResolverUtils.GetObjectSelector(attribute) ?? GetDefaultObjectSelector();
        }
    }
}
