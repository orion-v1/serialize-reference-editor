﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace Orion.SerializeReference.Editor
{
    [ObjectFactoryImplementation(typeof(InheritedObjectFactoryAttribute))]
    public class InheritedObjectFactory : IObjectFactory
    {
        public FieldInfo FieldInfo { get; set; }


        public IEnumerable<IObjectDescriptor> GetObjectDescriptors(Type targetType)
        {
            var attribute = FieldInfo.GetCustomAttribute<InheritedObjectFactoryAttribute>();
            if (attribute != null)
            {
                if (attribute.BaseType != null)
                {
                    if (targetType.IsAssignableFrom(attribute.BaseType)) 
                        targetType = attribute.BaseType;
                    else 
                        Debug.LogError($"Type {attribute.BaseType} is not assignable from {targetType}.");
                }
            }

            return AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(p => p.DefinedTypes)
                .Where(Condition)
                .Select(p => new Descriptor(p));

            bool Condition(Type type) =>
                type.IsClass && !type.IsAbstract && type.IsPublic &&
                targetType.IsAssignableFrom(type) &&
                !typeof(UnityEngine.Object).IsAssignableFrom(type);
        }

        public object CreateObject(IObjectDescriptor descriptor)
        {
            if (descriptor == null) return null;

            var type = (descriptor as Descriptor).Type;
            return Activator.CreateInstance(type);
        }


        class Descriptor : IObjectDescriptor
        {
            public Type Type { get; }

            public string Description => Type.FullName;

            public Descriptor(Type type)
            {
                Type = type;
            }
        }
    }
}
