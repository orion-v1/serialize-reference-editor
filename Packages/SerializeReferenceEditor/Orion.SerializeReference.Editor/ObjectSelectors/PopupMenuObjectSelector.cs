﻿using System;
using UnityEditor;
using UnityEngine;

namespace Orion.SerializeReference.Editor
{
    public class PopupMenuObjectSelector : IObjectSelector
    {
        private IObjectSelectorContext _context;

        public void Show(IObjectSelectorContext context)
        {
            _context = context;

            var descriptors = context.ObjectFactory.GetObjectDescriptors(context.TargetType);
            if (descriptors == null) return;


            var menu = new GenericMenu();
            
            // add null value
            menu.AddItem(new GUIContent("<Null>"), false, OnSelect, null);

            // add types
            foreach (var descriptor in descriptors)
            {
                menu.AddItem(
                    new GUIContent(descriptor.Description),
                    false,
                    OnSelect,
                    descriptor);
            }

            menu.DropDown(context.PropertyRect);
        }

        private void OnSelect(object item) => _context.SetResult(item as IObjectDescriptor);
    }
}
