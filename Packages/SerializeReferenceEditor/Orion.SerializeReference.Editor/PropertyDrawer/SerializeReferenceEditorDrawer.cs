﻿using System;
using UnityEditor;
using UnityEngine;

namespace Orion.SerializeReference.Editor
{
    [CustomPropertyDrawer(typeof(SerializeReferenceEditorAttribute))]
    public class SerializeReferenceEditorDrawer : PropertyDrawer
    {
        private static readonly int ControlHash = nameof(SerializeReferenceEditorDrawer).GetHashCode();
        private static readonly DefaultContractResolver _contactResolver = new DefaultContractResolver();
        
        private static GUIStyle _objectFieldButtonStyle;
        public static GUIStyle ObjectFieldButtonStyle => _objectFieldButtonStyle ?? (_objectFieldButtonStyle = new GUIStyle("ObjectFieldButton"));

        public static bool IsPropertyHeightIncludesChildren { get; set; } = true;

        public bool IsManagedReference(SerializedProperty property) => property.propertyType == SerializedPropertyType.ManagedReference;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return IsManagedReference(property)
                ? EditorGUI.GetPropertyHeight(property, IsPropertyHeightIncludesChildren)
                : EditorGUIUtility.singleLineHeight;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (!IsManagedReference(property))
            {
                EditorGUI.HelpBox(position, $"Property '{property.name}' is not a managed reference", MessageType.Error);
                return;
            }

            // draw property
            EditorGUI.BeginProperty(position, label, property);
            int id = GUIUtility.GetControlID(ControlHash, FocusType.Keyboard, position);

            Rect rect = EditorGUI.PrefixLabel(position, id, label);
            rect.height = EditorGUIUtility.singleLineHeight;
            
            Event e = Event.current;
            Data data = new Data
            {
                Property = property,
                PropertyRect = rect,
                ControlId = id,
                Event = e
            };

            switch (e.type)
            {
                case EventType.MouseDown: OnMouseDown(data); break;
                case EventType.KeyDown: OnKeyDown(data); break;
                case EventType.Repaint: OnRepaint(data); break;
            }

            EditorGUI.EndProperty();

            // draw the object properties if exists
            EditorGUI.PropertyField(position, property, GUIContent.none, true);
        }

        private Rect GetButtonRect(Rect rect) => new Rect(rect.xMax - 19, rect.y, 19, rect.height);

        private void OnMouseDown(Data data)
        {
            Rect buttonRect = GetButtonRect(data.PropertyRect);

            EditorGUIUtility.editingTextField = false;

            if (buttonRect.Contains(data.Event.mousePosition))
            {
                if (GUI.enabled)
                {
                    GUIUtility.keyboardControl = data.ControlId;
                    ShowObjectSelector(data);
                    data.Event.Use();
                    GUIUtility.ExitGUI();

                }
            }
            else if (data.PropertyRect.Contains(data.Event.mousePosition))
            {
                bool showSelector = data.Event.clickCount >= 2;
                GUIUtility.keyboardControl = data.ControlId;
                if (showSelector) ShowObjectSelector(data);
                data.Event.Use();
                if (showSelector) GUIUtility.ExitGUI();
            }
        }

        private void OnKeyDown(Data data)
        {
            if (GUIUtility.keyboardControl != data.ControlId) return;
            
            switch (data.Event.keyCode)
            {
                case KeyCode.Backspace:
                case KeyCode.Delete:
                    if (data.Property != null)
                    {
                        data.Property.managedReferenceValue = null;
                        data.Event.Use();
                        GUI.changed = true;
                    }
                    break;

                case KeyCode.Return:
                case KeyCode.Space:
                    ShowObjectSelector(data);
                    data.Event.Use();
                    GUIUtility.ExitGUI();
                    break;
            }
        }

        private void OnRepaint(Data data)
        {
            // resolve type name
            string typeName = data.Property.managedReferenceFullTypename;
            if (string.IsNullOrEmpty(typeName))
                typeName = "<Null>";
            else
            {
                int idx = typeName.LastIndexOf('.');
                if (idx >= 0) typeName = typeName.Substring(idx + 1);
            }

            // draw object field
            EditorStyles.objectField.Draw(
                data.PropertyRect,
                new GUIContent(typeName),
                data.ControlId,
                data.ControlId == DragAndDrop.activeControlID,
                data.PropertyRect.Contains(data.Event.mousePosition));

            // draw selector button
            Rect buttonRect = ObjectFieldButtonStyle.margin.Remove(GetButtonRect(data.PropertyRect));
            ObjectFieldButtonStyle.Draw(
                buttonRect, 
                GUIContent.none, 
                data.ControlId,
                data.ControlId == DragAndDrop.activeControlID,
                buttonRect.Contains(data.Event.mousePosition));
        }

        private void ShowObjectSelector(Data data)
        {
            SerializeReferenceProperty property = _contactResolver.ResolveProperty(fieldInfo);
            if (property == null) return;
            if (property.ObjectSelector == null) return;
            if (property.ObjectFactory == null) return;

            var context = new ObjectSelectorContext(
                property.ObjectFactory,
                property.TargetType,
                data.Property,
                data.PropertyRect,
                OnSelect);

            property.ObjectSelector.Show(context);


            void OnSelect(IObjectSelectorContext ctx, IObjectDescriptor descriptor)
            {
                var p = (ctx as ObjectSelectorContext).SerializedProperty;
                var obj = ctx.ObjectFactory.CreateObject(descriptor);

                p.serializedObject.UpdateIfRequiredOrScript();
                p.managedReferenceValue = obj;
                p.serializedObject.ApplyModifiedProperties();
                GUI.changed = true;
            }
        }


        private struct Data
        {
            public SerializedProperty Property;
            public Rect PropertyRect;
            public int ControlId;
            public Event @Event;
        }
    }
}
