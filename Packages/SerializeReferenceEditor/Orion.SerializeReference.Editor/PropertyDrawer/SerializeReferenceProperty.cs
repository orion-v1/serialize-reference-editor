﻿using System;
using System.Reflection;

namespace Orion.SerializeReference.Editor
{
    internal class SerializeReferenceProperty : ISerializeReferenceProperty
    {
        public FieldInfo FieldInfo { get; }
        
        public Type TargetType { get; set; }

        public bool IsCollection { get; set; }

        //public bool AllowNull { get; set; }

        public IObjectFactory ObjectFactory { get; set; }

        public IObjectSelector ObjectSelector { get; set; }


        public SerializeReferenceProperty(FieldInfo fieldInfo)
        {
            FieldInfo = fieldInfo;
        }
    }
}
