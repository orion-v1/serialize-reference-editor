﻿using System;

namespace Orion.SerializeReference
{
    public class InheritedObjectFactoryAttribute : ObjectFactoryAttribute
    {
        public Type BaseType { get; set; }
    }
}
