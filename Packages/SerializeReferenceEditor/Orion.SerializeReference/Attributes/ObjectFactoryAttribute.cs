﻿using System;

namespace Orion.SerializeReference
{
    [AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
    public abstract class ObjectFactoryAttribute : Attribute
    {
    }
}
