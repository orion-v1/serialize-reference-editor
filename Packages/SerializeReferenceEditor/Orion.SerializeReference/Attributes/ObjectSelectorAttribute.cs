﻿using System;

namespace Orion.SerializeReference
{
    [AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
    public class ObjectSelectorAttribute : Attribute
    {
    }
}
